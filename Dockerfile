FROM openjdk:11
MAINTAINER baeldung.com
COPY target/webScraping-0.0.1-SNAPSHOT.jar webScraping-1.0.0.jar
ENTRYPOINT ["java","-jar","/webScraping-1.0.0.jar"]