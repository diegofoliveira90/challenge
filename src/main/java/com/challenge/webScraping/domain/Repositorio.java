package com.challenge.webScraping.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Repositorio implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;	
	private String url;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "repositorio")
	private List<Item> itens = new ArrayList<Item>();
	
	public List<Item> getItens() {
		return itens;
	}
	public void setItens(List<Item> itens) {
		this.itens = itens;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Item getItemExtensao(String extensao) {
		for(Item item: this.itens) {
			if(item.getExtensao().equals(extensao))
				return item;
		}			
		return null;
	}
	
	public Repositorio(String url) {
		super();		
		this.url = url;		
	}
	
	public Repositorio() {
		
	}
	
}
