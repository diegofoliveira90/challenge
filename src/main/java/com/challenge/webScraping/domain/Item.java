package com.challenge.webScraping.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Item implements Serializable{	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String extensao;
	private Integer quantidade;
	private Integer linhas;
	
	

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "repositorio_id")
	private Repositorio repositorio;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Repositorio getRepositorio() {
		return repositorio;
	}
	public void setRepositorio(Repositorio repositorio) {
		this.repositorio = repositorio;
	}
	
	public Integer getLinhas() {
		return linhas;
	}
	public void setLinhas(Integer linhas) {
		this.linhas = linhas;
	}
	
	public Item(Integer id, String extensao, Integer quantidade,Integer linhas, Repositorio repositorio) {
		super();
		this.id = id;
		this.extensao = extensao;
		this.quantidade = quantidade;
		this.linhas = linhas;
		this.repositorio = repositorio;
	}
	
	public Item() {
		
	}
	
	

}
