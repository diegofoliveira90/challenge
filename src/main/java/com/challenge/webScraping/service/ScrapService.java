package com.challenge.webScraping.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.challenge.webScraping.DTO.ItemDTO;

@Service
public class ScrapService {
	
	
	
	public List<ItemDTO> consultar(String url) {
	       
		try {
			Document doc = Jsoup.connect(url).get();			
			
			Elements elementos = doc.getElementsByClass("Box-row");
			
			List<ItemDTO> lista = new ArrayList<ItemDTO>();
			
			lista.addAll(navegarPasta(doc));
			
			return lista;	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ItemDTO> validarArquivo(ItemDTO item) {
		
		try {
			List<ItemDTO> lista = new ArrayList<ItemDTO>();
			Pattern p = Pattern.compile("(\\d+)[^\\d]+lines");
			Pattern p2 = Pattern.compile("(\\d+)[^\\d]+KB");
				
			Document doc = Jsoup.connect(item.getUrl()).get();
			
			Elements elemento = doc.select("#repo-content-pjax-container > div > div.Box.mt-3.position-relative > div.Box-header.py-2.pr-2.d-flex.flex-shrink-0.flex-md-row.flex-items-center > div.text-mono.f6.flex-auto.pr-3.flex-order-2.flex-md-order-1");
			
			if(!elemento.isEmpty()) {
				item.setNome(doc.select("#blob-path > strong").text());
				item.setExtensao(FilenameUtils.getExtension(item.getNome()));
				Matcher m = p.matcher(elemento.text());
				Matcher m2 = p2.matcher(elemento.text());
				if(m.find()) {
					item.setQuantidade(item.getQuantidade() + Integer.parseInt(m.group(1)));	
				}else if(m2.find()) {
					item.setQuantidade(0);
				}
				lista.add(item);
			}else {
				//se não for um arquivo entao e uma pasta
				return navegarPasta(doc);
			}
			Thread.sleep(1000);
			return lista;		
		} catch (IOException | InterruptedException e) {		
			e.printStackTrace();	
			return null;
		}
	}
	
	private List<ItemDTO> navegarPasta(Document doc) {
		
		List<ItemDTO> lista = new ArrayList<ItemDTO>();
		Elements elementosNovos = doc.getElementsByClass("Box-row");
		
		for (Element elemento : elementosNovos) {
			ItemDTO itemNovo = new ItemDTO();
			
			itemNovo.setQuantidade(0);
			// Extraindo o titulo
			itemNovo.setNome(elemento.getElementsByClass("js-navigation-open Link--primary").text());
			//se nome for em branco e um retorno deve ser ignorado
			if(!itemNovo.getNome().isBlank()) {
				itemNovo.setUrl(elemento.select("a[href]").attr("abs:href"));
				lista.addAll(validarArquivo(itemNovo));	
			}
			
	        
	      }
		
		return lista;
	}
	
	
}
