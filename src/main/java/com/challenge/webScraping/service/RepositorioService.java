package com.challenge.webScraping.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.webScraping.DTO.ItemDTO;
import com.challenge.webScraping.DTO.RetornoDTO;
import com.challenge.webScraping.domain.Item;
import com.challenge.webScraping.domain.Repositorio;
import com.challenge.webScraping.repository.ItemRepository;
import com.challenge.webScraping.repository.RespositorioRepository;

@Service
public class RepositorioService {

	@Autowired
	private ScrapService scrapService;
	
	@Autowired
	private RespositorioRepository repoRepo;
	
	@Autowired
	private ItemRepository repoItem;
	
	public List<RetornoDTO> consultar(String url) {
		
		Repositorio repositorio = repoRepo.findByUrl(url);
		if(repositorio == null) {
			List<ItemDTO> lista = scrapService.consultar(url);	
			
			List<RetornoDTO> retorno = agruparPorExtensao(lista);
			create(retorno,url);
					
			return retorno;
		}
		return transformRepo(repositorio);
		
	}
	
	private List<RetornoDTO> agruparPorExtensao(List<ItemDTO> itens) {
		
		List<RetornoDTO> lista = new ArrayList<RetornoDTO>();
		for(ItemDTO item: itens) {
			perform(lista,item);
		}		
		return lista;
	}
	
	private void perform(List<RetornoDTO> list, ItemDTO item){
		if(list.stream().filter(o -> o.getExtension().equals(item.getExtensao())).findFirst().isPresent()) {
			list.stream().filter(o -> o.getExtension().equals(item.getExtensao())).forEach(
		            o -> {
		                o.setLines(o.getLines()+item.getQuantidade());
		                o.setCount(o.getCount() +1);
		            }
		    );
		}else {
			list.add(new RetornoDTO(item.getExtensao(), 1, item.getQuantidade()));
		}	    
	}
	
	private void create(List<RetornoDTO> list, String url) {
		List<Item> itens = new ArrayList<Item>();
		
		Repositorio repositorio = new Repositorio(url);
		repositorio = repoRepo.save(repositorio);
		
		for(RetornoDTO retorno: list) {
			Item temp =  new Item(null, retorno.getExtension(), retorno.getCount(), retorno.getLines(), repositorio);
			repoItem.save(temp);
		}
	}
	
	private List<RetornoDTO> transformRepo(Repositorio repo){
		List<RetornoDTO> lista = new ArrayList<RetornoDTO>();
		for(Item item:repo.getItens()) {
			lista.add(new RetornoDTO(item.getExtensao(), item.getQuantidade(), item.getLinhas()));
		}
		
		return lista;
	}
	
	public List<RetornoDTO> updateRepositorio(String url) {
		Repositorio repositorio = repoRepo.findByUrl(url);
		List<ItemDTO> lista = scrapService.consultar(url);			
		List<RetornoDTO> retorno = agruparPorExtensao(lista);
		
		for (RetornoDTO retornoDTO : retorno) {
			Item item = repositorio.getItemExtensao(retornoDTO.getExtension());
			if(item != null) {
				item.setLinhas(retornoDTO.getLines());
				item.setQuantidade(retornoDTO.getCount());
				repoItem.save(item);
			}else{
				Item temp =  new Item(null, retornoDTO.getExtension(), retornoDTO.getCount(), retornoDTO.getLines(), repositorio);
				repoItem.save(temp);
			}			
		}
		return transformRepo(repositorio);
	}
	
}
