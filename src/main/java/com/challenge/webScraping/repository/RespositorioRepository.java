package com.challenge.webScraping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.challenge.webScraping.domain.Repositorio;

@Repository
public interface RespositorioRepository extends JpaRepository<Repositorio, Integer>{
	
	@Transactional(readOnly = true)
	Repositorio findByUrl(String url);
	
}
