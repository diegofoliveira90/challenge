package com.challenge.webScraping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.webScraping.domain.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer>{

}
