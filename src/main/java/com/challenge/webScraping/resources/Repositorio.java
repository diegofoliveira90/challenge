package com.challenge.webScraping.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.webScraping.DTO.ConsultaDTO;
import com.challenge.webScraping.service.RepositorioService;

@RestController
@RequestMapping(value = "/repositorio")
public class Repositorio {

	@Autowired
	private RepositorioService repositorioService;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> insert(@RequestBody ConsultaDTO consulta){
		
		return ResponseEntity.ok().body(repositorioService.consultar(consulta.getUrl()));
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> Update(@RequestBody ConsultaDTO consulta){		
		return ResponseEntity.ok().body(repositorioService.updateRepositorio(consulta.getUrl()));
	}
	
}
