package com.challenge.webScraping;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.challenge.webScraping.DTO.ItemDTO;
import com.challenge.webScraping.service.ScrapService;

@SpringBootTest
class WebScrapingApplicationTests {

	@Autowired
	private ScrapService scrapService;
	
	
	@Test
	void validarItem() {
		
		ItemDTO item = new ItemDTO();
		item.setUrl("https://github.com/msolefonte/tww2-vco2-framework/blob/master/.gitignore");
		List<ItemDTO> lista = scrapService.validarArquivo(item);
		assertTrue(!lista.isEmpty());
	}
	
	@Test
	void validarPastas() {
		ItemDTO item = new ItemDTO();
		item.setUrl("https://github.com/msolefonte/tww2-vco2-framework/tree/master/src");
		List<ItemDTO> lista = scrapService.validarArquivo(item);
		assertTrue(!lista.isEmpty());
	}
	
	
	

}
