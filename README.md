# springboot-Challenge-Scraping

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Minimal [Spring Boot](http://projects.spring.io/spring-boot/) sample app.

## Requisitos

Para buildar a aplicação é necessario

- [JDK 11](https://www.oracle.com/java/technologies/downloads/#java11)
- [Maven 3](https://maven.apache.org)

## Rodando a aplicação local

Para utilizar o docker
```shell
mvn package
```

```shell
docker build --tag=webscraping:latest .
```

```shell
docker run -p 8080:8080 webscraping:latest .
```

pode ser utilizado o maven para executar local

```shell
mvn spring-boot:run
```

## PostMan Collection

[Collection](https://www.getpostman.com/collections/b7205831d39dac1aba4a)

## Curl para teste
curl --location --request POST 'http://localhost:8080/repositorio' \
--header 'Content-Type: application/json' \
--data-raw '{    
    "url":"https://github.com/msolefonte/tww2-vco2-framework"
}'


## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://github.com/codecentric/springboot-sample-app/blob/master/LICENSE) file.